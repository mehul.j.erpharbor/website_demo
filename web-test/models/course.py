from odoo import models, fields, api

class Courses(models.Model):
    _name = 'webtest.courses'
    # _inherit = 'mail.thread'
    #_inherit = 'product.template'

    name = fields.Char("Name")
    # category_courses = fields.Char("Category")
    person_id = fields.Many2one('webtest.persons',string="Person")