from odoo import models, fields, api

class Person(models.Model):
    _name = 'webtest.persons'

    name = fields.Char()
    biography = fields.Html()

    course_ids = fields.One2many('webtest.courses', 'person_id', string="Courses")