# -*- coding: utf-8 -*-
from odoo import http

class testcaptcha(http.Controller):
    @http.route('/mytest/testcaptcha/',auth='public',website=True, csrf=False)
    def index(self,**kw):
        return http.request.render('web-test.testcaptcha',{
        })

class Captcha_redirect(http.Controller):
    @http.route('/mytest/captcha_redirect/',auth='public',website=True,csrf=False)
    def index(self,**kw):
        return http.request.render('web-test.redirect',{
        })

class Mytest(http.Controller):
    @http.route('/mytest/', auth='public')
    def index(self, **kw):
        return "Hello, world"

# class Example(http.Controller):
#     @http.route('/mytest/one/',auth='public')
#     def index(self,**kw):
#         return http.request.render('web-test.index',{
#             'fields':["ABC","OPQ","XYZ"]
#         })

class Demo(http.Controller):
    @http.route('/mytest/test/', auth='public')
    def indexone(self, **kw):
        Persons = http.request.env['webtest.persons']
        return http.request.render('web-test.indexone', {
            'persons': Persons.search([])
        })

class Example(http.Controller):
    @http.route('/mytest/one/',auth='public',website=True)
    def index(self,**kw):
        Persons = http.request.env['webtest.persons']
        return http.request.render('web-test.index',{
            'persons' : Persons.search([])
        })

    # @http.route('/mytest/<name>/',auth='public',website=True)
    # def person(self,name):
    #     return '<h1>{}</h1>'.format(name)

    # @http.route('/mytest/<int:id>/', auth='public', website=True)
    # def person(self, id):
    #     return '<h1>{} ({})</h1>'.format(id,type(id).__name__)

    @http.route('/mytest/mytest/<model("webtest.persons"):person>/',auth='public',website=True)
    def person(self,person):
        return http.request.render('web-test.biography',{
            'test':person
        })